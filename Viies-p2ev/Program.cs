﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viies_p2ev
{
    class Program
    {
        static void Main(string[] args)
        {
            string failinimi = @"..\..\Test.txt";//nii saab faili panna ainult siis kui projektikaustas on fail
            //var  loetud = File.ReadAllText(failinimi);// see vastus on alati string
            //Console.WriteLine(loetud);
            var loetudRead = File.ReadAllLines(failinimi);// see vastus on alati stringi massiiv, saab aru reavahetusest
            for (int i = 0; i < loetudRead.Length; i++)
            {
                Console.WriteLine($"Rida {i}.... {loetudRead[i]}");
            }
            //string uusfailinimi = @"..\..\TestUus.txt";
            //File.WriteAllLines(uusfailinimi, loetudRead);

            Dictionary<string, int> nimekiri = new Dictionary<string, int>();
            foreach(var x in loetudRead)
            {
                var jupid = x.Split(' '); //split teeb alati stringide massiivi, tükid lõigatakse sealt kust sina ütled et tuleb tükeldada, hetkel tühiku koha pealt ja tükke tuleb alati nii palju kui tükke on + 1
                string nimi = jupid[0];
                int vanus = int.Parse(jupid[1]);
                nimekiri.Add(nimi, vanus);
            }
            {
                foreach (var x in nimekiri) Console.WriteLine(x);
                Console.WriteLine($"Noorim vanus on {nimekiri.Values.Min()}");
            }
        }
    }
}
