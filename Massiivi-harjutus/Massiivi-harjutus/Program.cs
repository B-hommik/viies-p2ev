﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Massiivi_harjutus
{
    class Program
    {
        static void Main(string[] args)
        {
            int ridu = 10;
            int veerge = 10;
            int[,] tabel = new int[ridu, veerge];
            Console.WriteLine(tabel.Rank);//saad tada mitmemõõtmeline list on
            Console.WriteLine(tabel.Length);//saad teada mitu elementi tabelis on
            Random r = new Random();
            for (int i = 0; i < ridu; i++)
            { //kui need loogelised sulud ära jätta, siis läheb tsükkel ühelauseliseks ja ridu ja veerge ei panda õigesti!!!!)
                for (int j = 0; j < veerge; j++)
                {
                    tabel[i, j] = r.Next(100) + 1;
                    Console.Write($"{ tabel[i, j]}\t");
                }
                Console.WriteLine();
            }
            while (true) // seda on vaja selleks et see oleks lõpmata tsükkel VÕIB OLLA KA FOR (BOOL kasKysimeEdasi = TRUE; kasKysimeEdasi;)
            {
                Console.Write("Mida otsime: ");
                if (int.TryParse(Console.ReadLine(), out int otsitav) && otsitav <= 100 && otsitav > 0)//out int vaatab kas tegu on üldse arvuga ja see otsitav jne asi vaatab kas arv jääb vahemikku 1-100
                {
                    for (int i = 0; i < ridu; i++)
                        for (int j = 0; j < veerge; j++)
                        {
                            if (tabel[i, j] == otsitav)
                            {
                                Console.WriteLine($"Leidsin {otsitav} {i + 1}. rea {j + 1}. veerust");
                            }
                        }

                    break;// sellega saab lõpmata tsüklist välja, see peab olema tsükli sees. Kui kasutame FOR tsüklit, siis siiia tuleb kasKysimeEdasi = False;
                }
                else
                { Console.WriteLine("See pole kahjuks õiges vahemikus, või number"); }
            }
        }
    }
}