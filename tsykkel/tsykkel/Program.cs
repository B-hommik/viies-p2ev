﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tsykkel
{
    class Program
    {
        static void Main(string[] args)
        {//TSÜKKEL AINULT FOR 'iga
            //int[] arvud = { 1, 2, 6, 4, 5, 8, 6, 9 };
            //int i = 0;
            //for (; arvud[i] != 6; i++)//!= teeme niikaua kuni pole kaheksa, siis kui on kaheksa siis lõpetame ära
            //{ }//need on vajalikud kuna muidu läheks CW tsükli sisse ja kõik väärtused enne breaki trükitaks välja
            //    Console.WriteLine($"kuue leidsin pesast {i+1}");

            //kui tahad et mõlemad kuued välja trükitakse
            int[] numbrid = { 1, 4, 6, 7, 6, 8 };
            int o = 0;
            for (;o < numbrid.Length ; o++)
            {
                if (numbrid[o] == 6) Console.WriteLine($"kuue leidsin pesast {o+1}");
            }

            //TSÜKKEL IFi ja FORiga
   
            //int[] nimekiri = { 1, 2, 6, 6, 7, 4, 5 };
            //int e = 0;
            //for (; e < nimekiri.Length; e++)
            //{
            //    if (nimekiri[e] == 6) break;
            //}
            //{
            //    Console.WriteLine($"number kuus asub kohal {e + 1}");
            //}
        }
    }
}
